package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var firstVariable:Double = 0.0
    private var secondVariable:Double = 0.0
    private var operation = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        equalButton.isClickable = false
        Button0.setOnClickListener(this)
        Button1.setOnClickListener(this)
        Button2.setOnClickListener(this)
        Button3.setOnClickListener(this)
        Button4.setOnClickListener(this)
        Button5.setOnClickListener(this)
        Button6.setOnClickListener(this)
        Button7.setOnClickListener(this)
        Button8.setOnClickListener(this)
        Button9.setOnClickListener(this)
    }

    fun countOccurrences(s: String, ch: Char): Int {
        return s.filter { it == ch }.count()
    }

    fun equal(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty() && value[value.length - 1].toString() != "." && countOccurrences(value, '.') < 2) {
            secondVariable = value.toDouble()
            if (operation == ":" && secondVariable==0.0){

            }else{
                var result:Double = 0.0

                if(operation == ":"){
                    result = firstVariable/secondVariable
                }else if (operation == "x"){
                    result = firstVariable*secondVariable
                }else if(operation == "-"){
                    result = firstVariable-secondVariable
                }else if (operation == "+"){
                    result = firstVariable+secondVariable
                }

                resultTextView.text = result.toString()
                equalButton.isClickable = false
                divideButton.isClickable = true
                multiplyButton.isClickable = true
                subtractionButton.isClickable = true
                sumButton.isClickable = true
            }

        }
    }

    fun divide(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty() && value[value.length - 1].toString() != "." && countOccurrences(value, '.') < 2){
            equalButton.isClickable = true
            operation = ":"
            firstVariable = value.toDouble()
            resultTextView.text = ""
            divideButton.isClickable = false
            multiplyButton.isClickable = false
            subtractionButton.isClickable = false
            sumButton.isClickable = false
        }

    }

    fun multiply(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty() && value[value.length - 1].toString() != "." && countOccurrences(value, '.') < 2){
            equalButton.isClickable = true
            operation = "x"
            firstVariable = value.toDouble()
            resultTextView.text = ""
            divideButton.isClickable = false
            multiplyButton.isClickable = false
            subtractionButton.isClickable = false
            sumButton.isClickable = false
        }
    }

    fun subtraction(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty() && value[value.length - 1].toString() != "." && countOccurrences(value, '.') < 2){
            equalButton.isClickable = true
            operation = "-"
            firstVariable = value.toDouble()
            resultTextView.text = ""
            divideButton.isClickable = false
            multiplyButton.isClickable = false
            subtractionButton.isClickable = false
            sumButton.isClickable = false
        }
    }

    fun sum(view: View){
        val value = resultTextView.text.toString()
        if(value.isNotEmpty() && value[value.length - 1].toString() != "." && countOccurrences(value, '.') < 2){
            equalButton.isClickable = true
            operation = "+"
            firstVariable = value.toDouble()
            resultTextView.text = ""
            divideButton.isClickable = false
            multiplyButton.isClickable = false
            subtractionButton.isClickable = false
            sumButton.isClickable = false
        }
    }

    fun dot(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()){
            resultTextView.text = resultTextView.text.toString() + "."
        }
    }

    fun delete(view: View){
        var value = resultTextView.text.toString()
        if(value.isNotEmpty()){
            resultTextView.text = value.substring(0,value.length -1)
        }

    }

    override fun onClick(v: View?) {
        val button = v as Button
        resultTextView.text = resultTextView.text.toString() + button.text.toString()

    }
}